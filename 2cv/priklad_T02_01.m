clear all %smaze vsechny promene
close all;
A1=1; f1=1; phi1=0;
A2=2; f2=2; phi2=-pi/2;

t=linspace(0,1); %vyrobi 100 linearne rozdelenych hodnot v intervalu 0 az 1

y1=A1*sin(2*pi*f1*t+phi1);
y2=A2*cos(2*pi*f2*t+phi2);

plot(t,y1,t,y2)
grid on
title('goniometricke funkce')
xlabel('t[s]')
ylabel('sin(t), cos(t)')